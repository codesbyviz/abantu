<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::any('/register', 'HomeController@index')->name('register');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/search', 'Search@search')->name('search');
Route::post('/search', 'Search@search')->name('search');
Route::get('/search-more', 'Search@searchaction')->name('searchaction');
Route::post('/search', 'Search@search')->name('search');
Route::resource('/tags', 'TagsController');
Route::resource('/staff', 'UserController');
Route::resource('/people', 'PersonController');
Route::resource('/contact-categories', 'PersonTypeController');
Route::resource('/organization', 'OrganizationController');
Route::resource('/events', 'EventController');
Route::resource('/skills', 'SkillController');
Route::resource('/interests', 'InterestController');
Route::resource('/event/event-dates', 'EventDateController');
Route::get('/event/event-date/export/attendees/{event}', 'EventDateController@export')->name('export-event-attendees');
Route::post('/event/event-date/spot/attendees/{event}', 'EventDateController@spot')->name('attendance.spot');
Route::resource('/event-date/attendance', 'EventAttendanceController');
Route::resource('/event-date/speakers', 'EventSpeakersController');
Route::resource('settings', 'SettingsController');

// Excel Import

Route::post('/import/people', 'PersonController@import')->name('people.import');
Route::post('/import/organizations', 'OrganizationController@import')->name('organization.import');
Route::post('/import/events', 'EventController@import')->name('event.import');
Route::post('/import/event-dates', 'EventDateController@import')->name('event-dates.import');
Route::post('/import/event-dates/attendance', 'EventAttendanceController@import')->name('event-dates.attandance.import');