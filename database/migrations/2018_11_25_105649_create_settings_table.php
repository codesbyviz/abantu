<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Settings;
class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('option_name');
            $table->longtext('option_value');
            $table->timestamps();
        });
        $settings = new Settings;
        $settings->option_name  =   "organization_name";
        $settings->option_value =   "Abantu";
        $settings->save();
        
        $settings = new Settings;
        $settings->option_name  =   "organization_licence_key";
        $settings->option_value =   "DEFAULT_KEY";
        $settings->save();
        
        $settings = new Settings;
        $settings->option_name  =   "theme_color";
        $settings->option_value =   "#000000";
        $settings->save();
        
        $settings = new Settings;
        $settings->option_name  =   "support_email";
        $settings->option_value =   "help@synamaticsmediastudio.com";
        $settings->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
