<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Organization;
class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('location');
            $table->string('type');
            $table->string('person')->nullable();
            $table->longtext('tags')->nullable();
            $table->timestamps();
        });
        
        $Organization = new Organization;
        $Organization->id       = '0';
        $Organization->name     = 'Default Organization';
        $Organization->location = 'Default Location';
        $Organization->type     = 'Company';
        $Organization->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
