<?php

namespace App\Http\Controllers;

use App\Event;
use App\Organization;
use App\tags;
use Illuminate\Http\Request;
use Importer;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::paginate(10);
        return view('event.index',['events'=>$events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organizations = Organization::all();
        return view('event.create',['organizations'=>$organizations]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'name'=>'required|string|max:255',
            'organization'=>'required|string|max:255',
            'tags'=>'string|max:255',
        ]);

        $event       = new Event;
        $event->name = $request->name;
        $event->organization = $request->organization;
        $event->tags = $request->tags;
        $event->save();
        return redirect()->back()->with(['success'=>"New Event:  $event->name ."]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $organizations = Organization::all();
        return view('event.show',['event'=>$event,"organizations"=>$organizations]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
       $request->validate([
            'name'=>'required|string|max:255',
            'organization'=>'required|string|max:255',
            'tags'=>'string|max:255',
        ]);

        $event->name = $request->name;
        $event->organization = $request->organization;
        $event->tags = $request->tags;
        foreach(explode(',',$request->tags) as $tag){
            $fs = tags::where('tag',$tag)->first();
            if(!$fs){
                $fd = new tags;
                $fd->tag = $tag;
                $fd->save();
            }
        }
        $event->save();
        return redirect()->back()->with(['success'=>"Update Success."]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
    }
   public function import(Request $request)
    {
        $file   = $request->file('upload');
        $fl     = $file->move('storage/temp',$file->getClientOriginalName());
        $excel = Importer::make('Excel');
        $excel->load($fl);
        $collection = $excel->getCollection();
        $fields     = $collection[0];
        $data       = [];
        for ($i=1; $i <count($collection) ; $i++) {
            $data[$i] = [];
            for ($s=0; $s <count($fields) ; $s++) {
                $data[$i][$fields[$s]] = $collection[$i][$s];
            }
        }
        foreach($data as $dat){
            $event = new Event;
            foreach ($dat as $key => $value) {
                if($key == 'organization'){
                    $ps = Organization::where('name',$value)->first();
                    if($ps){
                        $event->{$key} = $ps->id;
                    }
                }
                elseif($key == 'tags'){
                    $key = trim($key);
                    foreach(explode(',',$value) as $tag){
                        $fs = tags::where('tag',$tag)->first();
                        if(!$fs){
                            $fd = new tags;
                            $fd->tag = $tag;
                            $fd->save();
                        }
                    }
                    $event->{$key} = $value;
                }
                else{
                    $event->{$key} = $value;
                }
            }
            $event->save();
        }
        unlink($fl);
        return redirect()->back()->with('success', 'Upload and Import Sucess!');
    }
}
