<?php 
namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Cyberduck\LaravelExcel\Contract\SerialiserInterface;

class PeopleExport implements SerialiserInterface
{
    public function getData($data)
    {
        $row = [];

        $row[] = $data->name;
        $row[] = $data->email;
        $row[] = $data->mobile;
        $row[] = $data->Category->category;
        $row[] = $data->Organization->organization;
        $row[] = $data->position;

        return $row;
    }

    public function getHeaderRow()
    {
        return [
            'Name',
            'Email ID',
            'Mobile No',
            'Category',
            'Organization',
            'Position'
        ];
    }
}
