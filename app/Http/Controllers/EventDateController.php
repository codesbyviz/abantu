<?php

namespace App\Http\Controllers;

use App\EventDate;
use App\EventAttendance;
use App\Event;
use App\Person;
use App\tags;
use App\Organization;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Importer;

class EventDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::all();
        return view('event.event-dates.create',['events'=>$events]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $request->validate([
            'event'=>'required|string|max:255',
            'date_from'=>'required|date|max:255',
            'date_to'=>'required|date|max:255',
            'venue'=>'string|max:255',
        ]);

        $event       = new EventDate;
        $event->event = $request->event;
        $event->date_from = Carbon::parse($request->date_from);
        $event->date_to = Carbon::parse($request->date_to);
        $event->venue = $request->venue;
        $event->save();
        return redirect()->route('event-dates.show',[$event->id])->with(['success'=>"Created new Event date"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventDate  $eventDate
     * @return \Illuminate\Http\Response
     */
    public function show(EventDate $eventDate)
    {
        $pioe = [];
        foreach($eventDate->Attendes as $eventDat){
            $pioe[] = $eventDat->person;
        }
        $people = Person::whereNotIn('id',$pioe)->get();

        $speakers = [];
        foreach($eventDate->Speakers as $eventDat){
            $speakers[] = $eventDat->person;
        }
        $organizations = Organization::all();
        $skills = \App\Skill::where('parent',0)->get();
        $interests = \App\Interest::where('parent',0)->get();
        $types = \App\PersonType::all();


        $speaker = Person::whereNotIn('id',$speakers)->get();
        return view('event.event-dates.show',['event'=>$eventDate,'people'=>$people,'speakers'=>$speaker,"organizations" => $organizations,"skills" => $skills,"interests" => $interests,"types" => $types]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventDate  $eventDate
     * @return \Illuminate\Http\Response
     */
    public function edit(EventDate $eventDate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventDate  $eventDate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EventDate $eventDate)
    {
        $eventDate->date_from = Carbon::parse($request->date_from);
        $eventDate->date_to = Carbon::parse($request->date_to);
        $eventDate->venue = $request->venue;
        $eventDate->save();
        return redirect()->back()->with('success', 'Updated Date!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventDate  $eventDate
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventDate $eventDate)
    {
        //
    }
   public function import(Request $request)
    {
        $file   = $request->file('upload');
        $fl     = $file->move('storage/temp',$file->getClientOriginalName());
        $excel = Importer::make('Excel');
        $excel->load($fl);
        $collection = $excel->getCollection();
        $fields     = $collection[0];
        $data       = [];
        for ($i=1; $i <count($collection) ; $i++) {
            $data[$i] = [];
            for ($s=0; $s <count($fields) ; $s++) {
                $data[$i][$fields[$s]] = $collection[$i][$s];
            }
        }
        foreach($data as $dat){
            $event = new EventDate;
            $event->event = $request->event;
            foreach ($dat as $key => $value) {
                if($key == 'event'){
                    $ps = Event::where('name',$value)->first();
                    if($ps){
                        $event->{$key} = $ps->id;
                    }
                }
                elseif($key == 'date_from' || 'date_to'){
                    $event->{$key} = Carbon::parse($ps->id);
                }
                else{
                    $event->{$key} = $value;
                }
            }
            $event->save();
        }
        unlink($fl);
        return redirect()->back()->with('success', 'Upload and Import Sucess!');
    }
    public function Export($event)
    {
        $event = EventDate::findorfail($event);
        $csvExporter = new \Laracsv\Export();
        $csvExporter->build($event->Attendes, ['Person.name'=>"Name",'Person.email'=>"Email",'Person.mobile'=>"Mobile"]);
        return $csvExporter->download(now().'.csv');
    }
    public function spot(Request $request,$event)
    {
        $request->validate([
            "name"=>"required",
            "email"=>"required|unique:people|string|email",
            "mobile"=>"required",
            "gender"=>"required"
        ]);
        $person = new Person;
        $person->name = $request->name;
        $person->email = $request->email;
        $person->mobile = $request->mobile;
        $person->gender = $request->gender;
        $person->dob = Carbon::parse($request->dob);
        $person->organization = $request->organization;
        $person->position = $request->position;
        $person->type = $request->profession_type;
        $person->data = $request->data;
        $person->city = $request->city;
        $person->state = $request->state;
        $person->country = $request->country;
        $person->tags = $request->tags;
        $person->save();
        if($request->skills){
            foreach($request->skills as $skill){
                $sk = new \App\PersonSkill;
                $sk->skill = $skill;
                $sk->person = $person->id;
                $sk->save();
            }
        }
        if($request->interests){
            foreach($request->interests as $interest){
                $sk = new \App\PersonInterest;
                $sk->interest = $interest;
                $sk->person = $person->id;
                $sk->save();
            }
        }
        $request->tags = trim($request->tags);
        foreach(explode(',',$request->tags) as $tag){
            $fs = tags::where('tag',$tag)->first();
            if(!$fs){
                $fd = new tags;
                $fd->tag = $tag;
                $fd->save();
            }
        }
            $atten = new EventAttendance;
            $atten->person       = $person->id; 
            $atten->event_date   = $event;
            $atten->status       = '1';
            $atten->save();
       return redirect()->back()->with(['success'=>"Created user and Added Registration"]);

    }
}
