<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Organization;
use App\Event;
use App\Interest;
use App\User;
use App\Skill;
use App\PersonInterest;
use App\PersonSkill;
use App\PersonType;
use Exporter;
class Search extends Controller
{
    public function index($q = null,Request $request)
    {
        $data = [];
        if(!$q){
            $q = $request->q;
        }
        $people = Person::where('name',"LIKE", '%'.$q.'%')
                        ->orwhere('email',"LIKE", '%'.$q.'%')
                        ->orwhere('mobile',"LIKE", '%'.$q.'%')
                        ->orwhere('tags',"LIKE", '%'.$q.'%')
                        ->orwhere('data',"LIKE", '%'.$q.'%')
                        ->get();
        foreach ($people as $key) {
            $data[] = ['content'=>"$key->name $key->email <br> <small class=\"text-muted\">Person</small>","link"=> route('people.show',[$key->id,"search-form"=>$q])];
        }
        $organizations = Organization::where('name',"LIKE", '%'.$q.'%')
                                        ->orwhere('location',"LIKE", '%'.$q.'%')
                                        ->orwhere('location',"LIKE", '%'.$q.'%')
                                        ->orwhere('tags',"LIKE", '%'.$q.'%')
                                        ->get();
        foreach ($organizations as $key) {
            $data[] = ['content'=>"$key->name <br> <small class=\"text-muted\">Organization</small>","link"=>route('organization.show',[$key->id,"search-form"=>$q])];
        }
        $events = Event::where('name',"LIKE", '%'.$q.'%')
                        ->orwhere('tags',"LIKE", '%'.$q.'%')
                        ->get();
        foreach ($events as $key) {
            $data[] = ['content'=>"$key->name<br> <small class=\"text-muted\">Event</small>","link"=>route('events.show',[$key->id,"search-form"=>$q])];
        }
        $user = User::where('name',"LIKE", '%'.$q.'%')
                        ->orwhere('position',"LIKE", '%'.$q.'%')
                        ->orwhere('level',"LIKE", '%'.$q.'%')
                        ->orwhere('salary',"LIKE", '%'.$q.'%')
                        ->orwhere('email',"LIKE", '%'.$q.'%')
                        ->get();
        foreach ($user as $key) {
            $data[] = ['content'=>"$key->name<br> <small class=\"text-muted\">Staff</small>","link"=>route('staff.show',[$key->id,"search-form"=>$q])];
        }
         return response()->json($data);
    }

    public function search(Request $request, Person $user){
        
        $organizations  = Organization::all();
        $skills         = Skill::where('parent',0)->get();
        $interests      = Interest::where('parent',0)->get();
        $types          = PersonType::all();
        // return $request;
        // return $request->tags;
        $user = $user->newQuery();
        if ($request->has('interests')) {
            $user->whereHas('Interests', function ($query) use ($request) {
                $query->whereIn('interest', $request->input('interests'));
            });
        }
        if ($request->has('skills')) {
            $user->whereHas('Skills', function ($query) use ($request) {
                $query->whereIn('skill', $request->input('skills'));
            });
        }
        
        if ($request->has('tags') && !empty($request->tags)) {
            $user->where('tags',"LIKE","%$request->tags%");
        }

        if ($request->has('types')) {
            $user->whereIn('type',$request->types);
        }

        if ($request->has('name')  && !empty($request->name)) {
            $user->where('name',"LIKE","%$request->name%");
        }
        if ($request->has('city')  && !empty($request->city)) {
            $user->where('city',$request->city);
        }

        if ($request->has('state')  && !empty($request->state)) {
            $user->where('state',$request->state);
        }

        if($request->export && $request->export == true){
            $people = $user->get();
            $csvExporter = new \Laracsv\Export();
            $csvExporter->build($people, ['name'=>"Name",'email'=>"Email",'mobile'=>"Mobile","Category.category"=>"Category"]);
            return $csvExporter->download(now().'.csv');
        }
        $people = $user->paginate(10);
        return view("search",['organizations'=>$organizations,'people'=>$people,'skills'=>$skills,'types'=>$types,'interests'=>$interests]);
    }
    public function searchaction()
    {
        return view('search-more');
        
    }
}

