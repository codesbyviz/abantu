<?php

namespace App\Http\Controllers;

use App\Person;
use App\Organization;
use App\Skill;
use App\Interest;
use App\PersonType;
use App\PersonSkill;
use App\PersonInterest;
use App\tags;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Importer;


class PersonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $people = Person::all();
        return view("people.index",['people'=>$people]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $organizations = Organization::all();
        $skills = Skill::where('parent',0)->get();
        $interests = Interest::where('parent',0)->get();
        $types = PersonType::all();
        return view("people.create",['organizations'=>$organizations,'skills'=>$skills,'types'=>$types,'interests'=>$interests]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "email"=>"required|unique:people|string|email",
            "mobile"=>"required",
            "gender"=>"required"
        ]);
        $person = new Person;
        $person->name = $request->name;
        $person->email = $request->email;
        $person->mobile = $request->mobile;
        $person->gender = $request->gender;
        $person->dob = Carbon::parse($request->dob);
        $person->organization = $request->organization;
        $person->position = $request->position;
        $person->type = $request->profession_type;
        $person->data = $request->data;
        $person->city = $request->city;
        $person->state = $request->state;
        $person->country = $request->country;
        $person->tags = $request->tags;
        $person->save();
        if($request->skills){
            foreach($request->skills as $skill){
                $sk = new \App\PersonSkill;
                $sk->skill = $skill;
                $sk->person = $person->id;
                $sk->save();
            }
        }
        if($request->interests){
            foreach($request->interests as $interest){
                $sk = new \App\PersonInterest;
                $sk->interest = $interest;
                $sk->person = $person->id;
                $sk->save();
            }
        }
        $request->tags = trim($request->tags);
        foreach(explode(',',$request->tags) as $tag){
            $fs = tags::where('tag',$tag)->first();
            if(!$fs){
                $fd = new tags;
                $fd->tag = $tag;
                $fd->save();
            }
        }
        return redirect()->back()->with(['success'=>"$person->name Added"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function show(Person $person)
    {
        return view("people.show",['person'=>$person]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function edit(Person $person)
    {
        $organizations = Organization::all();
        $skills = Skill::where('parent',0)->get();
        $interests = Interest::where('parent',0)->get();
        $types = PersonType::all();
        return view("people.edit",['person'=>$person,'organizations'=>$organizations,'skills'=>$skills,'types'=>$types,'interests'=>$interests]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Person $person)
    {
        $request->validate([
            "name"=>"required",
            "email"=>"required|unique:people,email,$person->id|string|email",
            "mobile"=>"required",
            "gender"=>"required"
        ]);
        $person->name = $request->name;
        $person->email = $request->email;
        $person->mobile = $request->mobile;
        $person->gender = $request->gender;
        $person->dob = Carbon::parse($request->dob);
        $person->organization = $request->organization;
        $person->position = $request->position;
        $person->type = $request->profession_type;
        $person->data = $request->data;
        $person->city = $request->city;
        $person->state = $request->state;
        $person->country = $request->country;
        $person->tags = $request->tags;
        $person->save();
        PersonSkill::where('person',$person->id)->delete();
        PersonInterest::where('person',$person->id)->delete();
        if($request->skills){
            foreach($request->skills as $skill){
                $din = PersonSkill::where('person',$person->id)->where('skill',$skill)->first();
                if(!$din){
                    $sk = new PersonSkill;
                    $sk->skill = $skill;
                    $sk->person = $person->id;
                    $sk->save();
                }
            }
        }
        if($request->interests){
            foreach($request->interests as $interest){
                $sk = new PersonInterest;
                $sk->interest = $interest;
                $sk->person = $person->id;
                $sk->save();
            }
        }
        $request->tags = trim($request->tags);
        if(isset($request->tags) && $request->tags !=="" && !empty($request->tags)){
            foreach(explode(',',$request->tags) as $tag){
                $fs = tags::where('tag',$tag)->first();
                if(!$fs){
                    $fd = new tags;
                    $fd->tag = $tag;
                    $fd->save();
                }
            }
        }
        return redirect()->back()->with(['success'=>"$person->name Updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function destroy(Person $person)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Person  $person
     * @return \Illuminate\Http\Response
     */
    public function import(Request $request)
    {
        $file   = $request->file('upload');
        $fl     = $file->move('storage/temp',$file->getClientOriginalName());
        $excel = Importer::make('Csv');
        $excel->load($fl);
        $collection = $excel->getCollection();
        $fields     = $collection[0];
        $data       = [];
        for ($i=1; $i <count($collection) ; $i++) {
            $data[$i] = [];
            for ($s=0; $s <count($fields) ; $s++) {
                $data[$i][$fields[$s]] = $collection[$i][$s];
            }
        }
        for ($i=0; $i < count($data); $i++) {
            if(isset($data[$i]['email'])){
                $person = Person::where("email",trim($data[$i]['email']))->orWhere("mobile",trim($data[$i]['mobile']))->first();
                /*
                ** Check if Person Already Defined 
                */
                if(!$person){
                    /*
                    ** Check if type is assigned
                    */
                    if(isset($data[$i]['type'])){
                        $person_type = PersonType::where("category",$data[$i]['type'])->first();
                        if($person_type){
                            $data[$i]['type'] = $person_type->id;
                        }
                        else{
                            $data[$i]['type'] = 1;
                        }
                    }
                    /*
                    ** Check if organization is assigned
                    */
                    if(isset($data[$i]['organization'])){
                        $organization = Organization::where("name",$data[$i]['organization'])->first();
                        if($organization){
                            $data[$i]['organization'] = $organization->id;
                        }
                        else{
                            $data[$i]['organization'] = 0;
                        }
                    }
                    else{
                        $data[$i]['organization'] = 0;
                    }
                    /*
                    ** Check if dob is assigned
                    */
                    if(isset($data[$i]['dob'])){
                        $data[$i]['dob'] = Carbon::parse($data[$i]['dob']);
                    }
                    /*
                    ** Check if Tags are assigned
                    */
                    if(isset($data[$i]['tags']) && $data[$i]['tags'] !=="" && !empty($data[$i]['tags'])){
                        $data[$i]['tags'] = trim($data[$i]['tags']);
                        foreach(explode(',',$data[$i]['tags']) as $tag){
                            $fs = tags::where('tag',$tag)->first();
                            if(!$fs){
                                $fd = new tags;
                                $fd->tag = $tag;
                                $fd->save();
                            }
                        }
                    }
                }
                else{
                    unset($data[$i]);
                }
            }            
        }
        /*
        ** Create All Users
        */
        foreach ($data as $key) {
            $person = Person::where("email",trim($key['email']))->first();
            if(!$person){
                $person = new Person;
                var_dump($key);
                foreach ($key as $ds => $value) {
                    $person->{$ds} = $value;
                }
                if(!$person->type){
                    $person->type = 1;
                }
                $person->save();
            }
        }
        unlink($fl);
        return redirect()->back()->with('success', 'Upload and Import Sucess!');
    }
}