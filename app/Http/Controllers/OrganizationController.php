<?php

namespace App\Http\Controllers;

use App\Organization;
use App\Person;
use App\tags;
use Illuminate\Http\Request;
use Importer;


class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $organizations = Organization::paginate(20);
        return view('organizations.index',['organizations'=>$organizations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $people = Person::all();
        return view('organizations.create',['people'=>$people]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            "name"=>"required",
            "category"=>"required",
        ]);
        $organization = new Organization;
        $organization->name = $request->name;
        $organization->location = $request->location;
        $organization->type = $request->category;
        $organization->tags = $request->tags;
        $organization->person = $request->person;
        $organization->email = $request->email;
        $organization->phone = $request->phone;
        $organization->save();
        foreach(explode(',',$request->tags) as $tag){
            $fs = tags::where('tag',$tag)->first();
            if(!$fs){
                $fd = new tags;
                $fd->tag = $tag;
                $fd->save();
            }
        }
        return redirect()->back()->with(['success'=>"Added New Organization"]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        return view('organizations.show',['organization'=>$organization]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        $people = Person::all();
        return view('organizations.edit',['organization'=>$organization,'people'=>$people]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        $request->validate([
            "name"=>"required",
            "category"=>"required",
        ]);
        $organization->name = $request->name;
        $organization->location = $request->location;
        $organization->type = $request->category;
        $organization->tags = $request->tags;
        $organization->email = $request->email;
        $organization->phone = $request->phone;
        $organization->person = $request->person;
        $organization->save();
        foreach(explode(',',$request->tags) as $tag){
            $fs = tags::where('tag',$tag)->first();
            if(!$fs){
                $fd = new tags;
                $fd->tag = $tag;
                $fd->save();
            }
        }
        return redirect()->back()->with(['success'=>"Organization updated"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        //
    }
    public function import(Request $request)
    {
        $file   = $request->file('upload');
        $fl     = $file->move('storage/temp',$file->getClientOriginalName());
        $excel = Importer::make('Csv');
        $excel->load($fl);
        $collection = $excel->getCollection();
        $fields     = $collection[0];
        $data       = [];
        for ($i=1; $i <count($collection) ; $i++) {
            $data[$i] = [];
            for ($s=0; $s <count($fields) ; $s++) {
                $data[$i][$fields[$s]] = $collection[$i][$s];
            }
        }
        foreach($data as $dat){
            $organization = new Organization;
            foreach ($dat as $key => $value) {
                if($key == 'person'){
                    $ps = Person::where('email',$value)->first();
                    if($ps){
                        $organization->{$key} = $ps->id;
                    }
                }
                elseif($key == 'tags'){
                    foreach(explode(',',$value) as $tag){
                        $fs = tags::where('tag',$tag)->first();
                        if(!$fs){
                            $fd = new tags;
                            $fd->tag = $tag;
                            $fd->save();
                        }
                    }
                    $person->{$key} = $value;
                }
                else{
                    $organization->{$key} = $value;
                }
            }
            $organization->save();
        }
        unlink($fl);
        return redirect()->back()->with('success', 'Upload and Import Sucess!');
    }
}
