<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonSkill extends Model
{
    public function Skill()
    {
        return $this->belongsTo("App\Skill","skill","id");
    }
    public function Person()
    {
        return $this->belongsTo("App\Person","person","id");
    }
}
