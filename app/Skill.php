<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Skill extends Model
{
public function parent(){
    return $this->belongsTo('App\Skill','parent')->where('parent',0)->with('parent');
}

public function children()
{
    return $this->hasMany('App\Skill','parent')->with('children');
}
}
