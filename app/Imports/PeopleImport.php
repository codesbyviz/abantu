<?php

namespace App\Imports;

use App\Person;
use App\Organization;
use App\PersonType;
use Maatwebsite\Excel\Concerns\ToModel;

class PeopleImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $org        = Organization::where('name',$row[9])->first();
        $position   = PersonType::where('category',$row[10])->first();
        if($org){
            return new Person([
                'name'     => $row[0],
                'email'    => $row[1], 
                'mobile'   => $row[2], 
                'gender'   => $row[3], 
                'city'     => $row[4], 
                'state'    => $row[5], 
                'country'  => $row[6], 
                'tags'     => $row[7], 
                'position' => $row[8], 
                'organization' => $org->id, 
                'type'      => $position->id, 
           ]);
        }
        else{
            return false;
        }
                
    }
}
