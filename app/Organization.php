<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['name','location','type','tags'];
    
    public function Person()
    {
        return $this->hasOne("App\Person","id","person");
    }
    public function Employees()
    {
        return $this->hasMany("App\Person","organization","id");
    }
    public function Events()
    {
        return $this->hasMany("App\Event","organization","id");
    }
}
