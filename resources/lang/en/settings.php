<?php 
use App\Settings;
$data = [];
foreach (Settings::all() as $key) {
    $data["int_$key->option_name"] = $key->option_value;
}
$data['abantu']                     = "Abantu";
$data['organization_name']          = "Organization Name";
$data['organization_licence_key']   = "Licence";
$data['theme_color']                = "Theme Color";
$data['support_email']              = "Support Email";
$data['menu_side_bar']              = [
                                        ["title"=>"Dashboard","route"=>"home"],
                                        ["title"=>"Interests","route"=>"interests.index"],
                                        ["title"=>"Skills","route"=>"skills.index"],
                                        ["title"=>"Staff","route"=>"staff.index"],
                                        ["title"=>"People","route"=>"people.index"],
                                        ["title"=>"Organizations","route"=>"organization.index"],
                                        ["title"=>"Events","route"=>"events.index"],
                                        ["title"=>"Contact Categories","route"=>"contact-categories.index"],
                                        ["title"=>"Tags","route"=>"tags.index"],
                                        ["title"=>"Settings","route"=>"settings.index"],
                                        ];
return $data;