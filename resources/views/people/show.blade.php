@extends('layouts.internal')
@section('title',$person->name)
@section('content')
<div class="page-header">
   <h3 class="mb-2">{{$person->name}} <br> <small>{{$person->email}}</small></h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('people.index')}}" class="breadcrumb-link">People</a></li>
               <li class="breadcrumb-item"><a href="" class="breadcrumb-link">{{$person->name}}</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="simple-card">
   <ul class="nav nav-tabs" id="tab-person-top" role="tablist">
       <li class="nav-item">
           <a class="nav-link border-left-0 active show" id="home-tab-simple" data-toggle="tab" href="#home-simple" role="tab" aria-controls="home" aria-selected="false">About</a>
       </li>
       <li class="nav-item">
           <a class="nav-link" id="profile-tab-simple" data-toggle="tab" href="#profile-simple" role="tab" aria-controls="profile" aria-selected="true">Events</a>
       </li>
       <li class="nav-item">
           <a class="nav-link" href="{{route('people.edit',$person->id)}}" role="tab" aria-controls="profile" aria-selected="true">Edit</a>
       </li>
       <li class="nav-item">
           <form action="{{route('people.destroy',$person->id)}}" method="post"><button type="submit" class="nav-link btn bg-danger text-white">Delete</button>@csrf<input type="hidden" name="_method" value="DELETE"></form>
       </li>
   </ul>
   <div class="tab-content" id="person-tab">
       <div class="tab-pane  fade active show" id="home-simple" role="tabpanel" aria-labelledby="home-tab-simple">
            <table class="table">
                <tr>
                    <th>Name</th>
                    <td>{{$person->name}}</td>
                </tr>
                <tr>
                    <th>Email ID</th>
                    <td>{{$person->email}}</td>
                </tr>
                <tr>
                    <th>Mobile No</th>
                    <td>{{$person->mobile}}</td>
                </tr>
                <tr>
                    <th>Gender</th>
                    <td>{{$person->gender}}</td>
                </tr>
                <tr>
                    <th>Date of Birth</th>
                    <td>{{Carbon::parse($person->dob)->toFormattedDateString()}} <br><i><small>{{Carbon::parse($person->dob)->diffInYears()}} years old</small></i></td>
                </tr>
                <tr>
                    <th>City</th>
                    <td>{{$person->city}}</td>
                </tr>
                <tr>
                    <th>State</th>
                    <td>{{$person->state}}</td>
                </tr>
                <tr>
                    <th>Country</th>
                    <td>{{$person->country}}</td>
                </tr>
                <tr>
                    <th>Profession</th>
                    <td>{{$person->position}}</td>
                </tr>
                <tr>
                    <th>Tags</th>
                    <td>
                        @forelse (explode(',',$person->tags) as $tag)
                            <span class="badge badge-success">{{$tag}}</span>
                        @empty
                            
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <th>Skills</th>
                    <td>
                        @forelse ($person->Skills as $skill)
                            <span class="badge badge-success">{{$skill->Skill->skill}}</span>
                        @empty
                            
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <th>Interests</th>
                    <td>
                        @forelse ($person->Interests as $interest)
                            <span class="badge badge-success">{{$interest->Interest->interest}}</span>
                        @empty
                            
                        @endforelse
                    </td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{{$person->CategoryName()}}</td>
                </tr>
                <tr>
                    <th>Organization</th>
                    <td>
                        @if($person->Organization)
                            <a href="{{route('organization.show',$person->Organization->id)}}">{{$person->Organization->name}}</a></td>
                        @else
                        No Organization
                        @endif
                </tr>
                @if($person->data)
                    @forelse (json_decode($person->data) as $title => $name)
                    <tr>
                        <th>{{$title}}</th>
                        <td>{{$name}}</td>
                    </tr>                            
                    @empty
                        
                    @endforelse
                @endif
            </table>
       </div>
       <div class="tab-pane" id="profile-simple" role="tabpanel" aria-labelledby="profile-tab-simple">
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th>Event</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($person->Events as $event)
                        <tr>
                            <th>
                                <a href="{{route('events.show',$event->EventDate->Event->id)}}">{{$event->EventDate->Event->name}}</a>
                                on
                                <a href="{{route('event-dates.show',$event->EventDate->id)}}">
                                @if (Carbon::parse($event->EventDate->date_from)->diffInDays($event->EventDate->date_to) == 0)
                                {{Carbon::parse($event->EventDate->date_from)->toFormattedDateString()}}
                                @else
                                {{Carbon::parse($event->EventDate->date_from)->toFormattedDateString()}} to {{Carbon::parse($event->EventDate->date_to)->toFormattedDateString()}} 
                                @endif
                                </a>
                                at {{$event->EventDate->venue}}
                                <br>
                                <small class="text-black-50">{{Carbon::parse($event->EventDate->date_from)->diffForHumans()}}</small>
                            </th>
                            <td>
                        @switch($event->status)
                            @case(1)
                                <span class="text-secondary">Registered</span>
                                @break
                            @case(2)
                                <span class="text-success">Attended</span>
                                @break
                            @case(3)
                                <span class="text-danger">Cancelled</span>
                                @break
                            @default
                                <span class="text-secondary">Status not available</span>
                            @endswitch
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
       </div>
   </div>
</div>

@endsection
