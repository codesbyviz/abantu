@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form class="card" action="{{route('organization.update',$organization->id)}}" method="POST">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="card-header">Edit {{$organization->name}}
                    <div class="float-right">
                        <a href="{{route('organization.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
                    </div>
                </div>
                <div class="card-body row">
                    <div class="form-group col-sm-12">
                        <label for="org_name">Name</label>
                        <input type="text" name="name" id="org_name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$organization->name}} @endif">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_location">Location</label>
                        <input type="text" name="location" id="org_location" class="form-control" value="@if(old('location')) {{old('location')}} @else {{$organization->location}}  @endif">
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_category">Category</label>
                        <select name="category" id="org_category" class="form-control">
                            <option @if(old('category') == 'School' || $organization->type =='School') selected @endif value="School">School</option>
                            <option @if(old('category') == 'College' || $organization->type =='College') selected @endif value="College">College</option>
                            <option @if(old('category') == 'Institution' || $organization->type =='Institution') selected @endif value="Institution">Institution</option>
                            <option @if(old('category') == 'Non-Profit' || $organization->type =='Non-Profit') selected @endif value="Non-Profit">Non-Profit</option>
                            <option @if(old('category') == 'Company' || $organization->type =='Company') selected @endif value="Company">Company</option>
                            <option @if(old('category') == 'Startup' || $organization->type =='Startup') selected @endif value="Startup">Startup</option>
                            <option @if(old('category') == 'Goverment' || $organization->type =='Goverment') selected @endif value="Goverment">Goverment</option>
                        </select>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="org_person">Contact Endpoint</label>
                        <select name="person" id="org_person" class="form-control">
                            @foreach ($people as $person)
                                <option @if(old('person') == $person->id || $organization->person == $person->id ) selected @endif value="$person->id">{{$person->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="org_tags">Tags</label>
                        <input type="text" name="tags" id="org_tags" class="form-control typeahead" value="@if(old('tags')) {{old('tags')}} @else {{$organization->tags}}  @endif">
                    </div>
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-outline-success btn-sm float-right">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
