@extends('layouts.internal')

@section('title',"Settings")
@section('content')
<div class="page-header">
   <h3 class="mb-2">Settings</h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('settings.index')}}" class="breadcrumb-link">Settings</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row">
        <form class="col-sm-12 col-md-4 card card-body" action="{{route('settings.store')}}" method="POST">
            @csrf
            @foreach ($settings as $setting)
            <div class="form-group">
                <label for="{{$setting->option_name}}">{{__("settings.$setting->option_name")}}</label>
                <input type="text" name="setting_{{$setting->id}}" id="{{$setting->option_name}}" class="form-control @if($setting->option_name == "theme_color") selector @endif" value="{{$setting->option_value}}" autocomplete="off">
            </div>
            @endforeach
            <div class="form-group">
                <button class="btn btn-info btn-sm float-right" type="submit">Save</button>
            </div>
        </form>
        <div class="col-sm-12 col-md-6">
            <div class="card card-body">
                <h1 class="display-7">{{__("settings.abantu")}} {{config('services.abantu.version')}}</h1>
                <div class="update-status">
                    
                </div>
                <button class="btn btn-outline-info btn-sm" id="check-for-updates-button">Check for Updates</button>
            </div>
        </div>
    </div>
</div>
<script>
$(function(){
        $(".selector").minicolors();
    $(document).on("click","#check-for-updates-button",function(e){
        e.preventDefault();
        $.ajax({
            url:"/updates.php",
            dataType:'json',
            success:function(data){
                if(data.version == "{{config('services.abantu.version')}}"){
                    $(".update-status").html(`<div class="alert alert-success">Your System is running the latest version.</div>`);
                }
                else if(data.version > "{{config('services.abantu.version')}}"){
                    $(".update-status").html(`<div class="alert alert-info">New Version Update available. Update to `+data.version+`</div>`);
                }
            }
        })
    })

})
</script>
@endsection