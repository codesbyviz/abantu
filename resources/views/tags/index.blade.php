@extends('layouts.internal')

@section('content')
<div class="page-header">
    <a href="{{route('tags.create')}}" class="btn btn-outline-info btn-sm float-right">Add Tag</a>
    <h3 class="mb-2">Tags <i>({{$tags->total()}})</i></h3>
    <div class="page-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="" class="breadcrumb-link">Tags</a></li>
            </ol>
        </nav>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            {{$tags->links()}}
        </div>
        <div class="col-sm-12 card">
            <table class="table sm">
                <thead>
                    <tr>
                        <th>Tag</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($tags as $tag)
                        <tr>
                            <th>
                               <a href="{{route('tags.show',[$tag->id])}}">{{$tag->tag}}</a>
                            </th>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection