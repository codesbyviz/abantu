@extends('layouts.internal')
@section('title',$tag->tag)

@section('content')
<div class="page-header">
    <h3 class="mb-2">{{$tag->tag}}</i></h3>
    <div class="page-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('tags.index')}}" class="breadcrumb-link">Tags</a></li>
                <li class="breadcrumb-item"><a href="" class="breadcrumb-link">{{$tag->tag}}</a></li>
            </ol>
        </nav>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card card-body">
                <h5>Events</h5>
                <table class="table sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($events as $event)
                        <tr>
                            <th><a href="{{route('events.show',[$event->id])}}">{{$event->name}}</a></th>
                        </tr>
                        @empty
                        <tr>
                            <th>No Events</th>
                        </tr>   
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-6 ">
            <div class="card card-body">
                <h5>People</h5>
                <table class="table sm">
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($people as $person)
                        <tr>
                            <th><a href="{{route('people.show',[$person->id])}}">{{$person->name}}</a></th>
                        </tr>
                        @empty
                        <tr>
                            <th>No People</th>
                        </tr>   
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection