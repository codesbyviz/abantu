@extends('layouts.internal')
@section('content')
    <div class="py-4"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <form action="#" method="POST" class="card card-body" id="search-form">
                    <div class="form-group">
                        <input type="search" name="q" id="q" class="form-control" value="" autocomplete="off">
                    </div>
                </form>
            </div>
            <div class="col-md-12" id="cont-search">
            </div>
        </div>
    </div>
    <script>
        $(function(){
            $(document).on("submit","#search-form",function(e){
                e.preventDefault();
                $.ajax({
                    url:"{{route('api.search')}}",
                    method:"GET",
                    data:$("#search-form").serialize(),
                    beforeSend:function(){
                        document.querySelector("#cont-search").innerHTML = "";
                        $("#cont-search").append(`<div class="loader"></div>`);
                    },
                    success:function(data){
                        document.querySelector("#cont-search").innerHTML = "";
                        data.forEach(element => {
                            $("#cont-search").append(`<div class="card card-body"><a href="`+element.link+`"><h5>`+element.content+`</h5></a></div>`);
                        });
                        if(data.length == 0){
                            $("#cont-search").append(`<div class="card card-body"><div class="text-center"><h2 class="display-4 material-icons">history</h2><p>No Results Found.</p></div></div>`);
                        }
                    }
                })
            })
        })
    </script>
@endsection