@extends('layouts.internal')
@section('title',"Search")
@section('content')
<div class="page-header">
    <h3 class="mb-2">Search <i>({{$people->total()}})</i></h3>
    <div class="page-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('search')}}" class="breadcrumb-link">Search</a></li>
            </ol>
        </nav>
    </div>
 </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-12">
                <div class="card card-body">
                <form action="{{route('search')}}" method="POST">
                    @csrf
                        <div class="form-group col-sm-12">
                            <label for="state">Name</label>
                            <input type="text" name="name" id="name" class="form-control" value="@if(Request::get('name')) {{Request::get('name') }}@endif">
                        </div>

                        <div class="form-group col-sm-12">
                            <label for="interests">Interests</label>
                            <select name="interests[]" multiple id="interests" class="form-control">
                                @foreach ($interests as $interest)
                                    <optgroup label="{{$interest->interest}}">
                                        @foreach ($interest->children as $child)
                                            <option 
                                            @if(Request::get('interests'))
                                            @foreach (Request::get('interests') as $item)
                                                @if($item == $child->id) selected @endif 
                                            @endforeach
                                            @endif                                            
                                            value="{{$child->id}}">{{$child->interest}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="skills">Skills</label>
                            <select name="skills[]" multiple id="skills" class="form-control">
                                @foreach ($skills as $skill)
                                    <optgroup label="{{$skill->skill}}">
                                        @foreach ($skill->children as $child)
                                            <option 
                                            @if(Request::get('skills'))
                                            @foreach (Request::get('skills') as $item)
                                                @if($item == $child->id) selected @endif 
                                            @endforeach
                                            @endif
                                            value="{{$child->id}}">{{$child->skill}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="types">Type</label>
                            <select name="types[]" multiple id="types" class="form-control">
                                @foreach ($types as $type)
                                    <option @if(Request::get('types'))
                                            @foreach (Request::get('types') as $item)
                                                @if($item == $type->id) selected @endif 
                                            @endforeach
                                            @endif
                                            value="{{$type->id}}">{{$type->category}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="city">City</label>
                            <input type="text" name="city" id="city" class="form-control" value="@if(Request::get('city')) {{Request::get('city') }}@endif">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="state">State</label>
                            <input type="text" name="state" id="state" class="form-control" value="@if(Request::get('state')) {{Request::get('state') }}@endif">
                        </div>
                        <div class="form-group col-sm-12">
                            <label for="tags">Tags</label>
                            <input type="text" name="tags" id="tags" class="form-control tags-item" value="@if(Request::get('tags')) {{Request::get('tags') }}@endif">
                        </div>
                        <div class="form-group col-sm-12">
                            <label class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" name="export" value="true" class="custom-control-input"><span class="custom-control-label">Export Results</span>
                            </label>
                        </div>
                        <div class="form-group col-sm-12">
                            <button type="submit" class="btn btn-outline-success">Filter</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-8">
                @forelse($people as $person)
                <div class="card card-body mb-1 py-1">
                    <a href="{{route('people.show',[$person->id])}}"><h5>{{$person->name}}</h5></a>
                    <small>{{$person->email}}</small>
                </div>
                @empty
                <div class="text-center text-black-50">
                    <h2 class="display-4 material-icons">history</h2>
                    <p>No Results Found.</p>
                </div>
                @endforelse
                <div class="py-2"></div>
                @if ($people)
                    {{ $people->links() }}
                @endif
            </div>
        </div>
    </div>
@endsection