@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Staff
                    <a href="{{route('staff.create')}}" class="btn btn-sm btn-outline-secondary float-right">Create New</a>
                </div>
                <table class="table table-sm">
                    <thead class="table-secondary">
                        <tr>
                            <th>Name</th>
                            <th>Email ID</th>
                            <th>Level</th>
                            <th>Position</th>
                            <th>Salary</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($employees as $employee)
                            <tr>
                                <th><a href="{{route('staff.show',$employee->id)}}">{{$employee->name}}</a></th>
                                <th>{{$employee->email}}</th>
                                <th>{{$employee->level}}</th>
                                <th>{{$employee->position}}</th>
                                <th>{{$employee->salary}}</th>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="4" class="text-center">No employees</th>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
