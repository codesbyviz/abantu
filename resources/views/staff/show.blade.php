@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{$employee->name}}
                    <a href="{{route('staff.index')}}" class="btn btn-sm btn-outline-secondary float-right">Back</a>
                </div>
                <form action="{{route('staff.update',[$employee->id])}}" class="card-body" method="POST">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        <label for="name">Full Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="@if(old('name')) {{old('name')}} @else {{$employee->name}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="email">Email ID</label>
                        <input type="text" name="email" id="email" class="form-control" value="@if(old('email')) {{old('email')}} @else {{$employee->email}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="level">Level of Administration</label>
                        <select name="level" id="level" class="form-control">
                            <option @if(old('level') == "Super Admin" || $employee->level == "Super Admin") selected @endif value="Super Admin">Super Admin</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="position">Position</label>
                        <input type="text" name="position" id="position" class="form-control" value="@if(old('position')) {{old('position')}} @else {{$employee->position}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary</label>
                        <input type="text" name="salary" id="salary" class="form-control" value="@if(old('salary')) {{old('salary')}} @else {{$employee->salary}}@endif">
                    </div>
                    <div class="form-group">
                        <label for="password">Change Password (leave empty for no Change)</label>
                        <input type="password" name="password" id="password" class="form-control" >
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-outline-success btn-sm">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
