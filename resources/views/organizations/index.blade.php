@extends('layouts.internal')
@section('title',"Organizations")
@section('content')
<div class="page-header">
   <h3 class="mb-2">Organizations <i>({{$organizations->total()}})</i></h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('organization.index')}}" class="breadcrumb-link">Organizations</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row" id="people-section">
        <div class="col-sm-12 col-md-10">
            <div class="card table-responsive card-body">
                <table class="table table-sm">
                    <thead class="table-secondary">
                        <tr>
                            <th>Name</th>
                            <th>Location</th>
                            <th>No of People</th>
                            <th>Email</th>
                            <th>Phone</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($organizations as $organization)
                            <tr>
                                <th><a href="{{route('organization.show',$organization->id)}}">{{$organization->name}}</a></th>
                                <th>{{$organization->location}}</th>
                                <th>{{$organization->Employees->count()}}</th>
                                <th>{{$organization->email}}</th>
                                <th>{{$organization->phone}}</th>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="4" class="text-center">No organizations</th>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
                <div class="py-1"></div>
                <div class="card-footer">
                    {{$organizations->links()}}
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-2">
            <div class="card card-body">
                <a href="{{route('organization.create')}}" class="btn btn-sm btn-outline-secondary">Create New</a>
                <br>
                <a href="#organization-upload-modal" data-toggle="modal" data-target="#organization-upload-modal" class="btn btn-sm btn-outline-primary">Upload CSV</a>
                <br>
                <a href="{{Storage::url('downloads/organization.csv')}}" class="btn btn-sm btn-outline-info">Template</a>
                </div>
            </div>
        </div>
    </div>
    <form action="{{route('organization.import')}}" method="POST" class="modal" id="organization-upload-modal" enctype="multipart/form-data">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    @csrf
                    <div class="form-group">
                        <input type="file" accept=".csv" required  name="upload" class="form-control">
                    </div>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-info btn-sm" type="submit">Upload</button>
            </div>
        </div>
    </form>
@endsection
