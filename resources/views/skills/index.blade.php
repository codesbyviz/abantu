@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('skills.create')}}" class="btn btn-outline-info btn-sm float-right">Add Skill</a>
                    Skills
                </div>
                <div class="card-body">
                    <table class="table sm">
                        <thead>
                            <tr>
                                <th>Skill</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($skills as $skill)
                            <tr>
                                <td><a href="{{route('skills.show',$skill->id)}}">{{$skill->skill}}</a></td>
                                <td>
                                <form action="{{route('skills.destroy',$skill->id)}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </form>
                            </td>
                            </tr>
                            @foreach ($skill->children as $child)
                            <tr>
                                <td><a href="{{route('skills.show',$child->id)}}">{{$skill->skill}} -> {{$child->skill}}</a></td>
                                <td>
                                <form action="{{route('skills.destroy',$child->id)}}" method="POST">
                                    @csrf
                                    <input type="hidden" name="_method" value="DELETE">
                                    <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                </form>
                            </td>
                            </tr>
                            @endforeach
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection