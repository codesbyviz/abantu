@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('skills.create')}}" class="btn btn-outline-info btn-sm float-right">Add Skill</a>
                     @if ($skill->parent)
                        {{$skill->Parent->skill}} -
                    @endif
                    {{$skill->skill}}
                </div>
                <div class="card-body">
                   <table class="table-sm">
                       @if ($skill->parent == 0)
                            @forelse ($skill->Children as $child)
                                <tr>
                                    <th>{{$child->skill}}</th>
                                </tr>
                            @empty
                                
                            @endforelse
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection