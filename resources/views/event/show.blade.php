@extends('layouts.internal')
@section('title',$event->name)
@section('content')
<div class="page-header">
    <h3 class="mb-2">{{$event->name}}</h3>
    <div class="page-breadcrumb">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
                <li class="breadcrumb-item"><a href="{{route('events.index')}}" class="breadcrumb-link">Events</a></li>
                <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">{{$event->name}}</a></li>
            </ol>
        </nav>
    </div>
 </div>
 <div class="container">
    <div class="row">

        <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5 class="text-muted">Total Dates</h5>
                        <h2 class="mb-0">{{$event->Dates->count()}} </h2>
                    </div>
                    <div class="float-right icon-circle-medium  icon-box-lg  bg-primary-light mt-1">
                        <i class="material-icons text-primary">calendar_today</i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-12"></div>
        <div class="col-sm-12 col-md-6">
            <div class="card table-responsive">
                <div class="card-header">
                    <a href="{{route('event-dates.create',['event'=>$event->id])}}" class="btn btn-xs btn-outline-danger float-right">Add New Date</a>
                    Event Dates
                </div>
                <table class="table table-xs">
                    @forelse ($event->Dates as $dates)
                    <tr>
                        <th> <a href="{{route('event-dates.show',$dates->id)}}">{{$dates->isDate()}}, {{$dates->venue}} </a></th>
                    </tr>
                    @empty
                        <th class="text-center" colspan="2">No Dates Announced</th>
                    @endforelse
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-4">
            <div class="card table-responsive">
                <div class="card-header">
                    <a href="#edit-event" data-toggle="modal" data-target="#edit-event" class="btn btn-xs btn-outline-danger float-right">Edit</a>
                    About Event
                </div>
                <table class="table table-xs">
                    <tbody>
                        <tr>
                            <th>Event Name</th>
                            <td>{{$event->name}}</td>
                        </tr>
                        <tr>
                            <th>Event Organizer</th>
                            <td><a href="{{route('organization.show',$event->Organizer->id)}}">{{$event->Organizer->name}}</a></td>
                        </tr>
                        <tr>
                            <th>Tags</th>
                            <td>{!!$event->Tags()!!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-2 ">
            <div class="card card-body">
                <a href="{{route('events.index')}}" class="btn btn-sm btn-outline-secondary">Back to List</a>
            </div>
        </div>
    </div>
 </div>
 <div class="modal" id="edit-event">
     <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                @include('event.inc.edit')
            </div>
        </div>
     </div>
 </div>
@endsection
