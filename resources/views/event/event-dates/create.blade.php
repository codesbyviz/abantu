@extends('layouts.internal')

@section('content')
<div class="container">
    <div class="py-4"></div>
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Add New Event Date
                    <div class="float-right">
                        @if (app('request')->input('event'))
                            <a href="{{route('events.show',app('request')->input('event'))}}" class="btn btn-sm btn-outline-secondary">Back to Event</a>
                        @else
                            <a href="{{route('event-dates.index')}}" class="btn btn-sm btn-outline-secondary">Back to Event Dates List</a>                            
                        @endif
                    </div>
                </div>
                <form class="card-body" action="{{route('event-dates.store')}}" method="POST">
                    @csrf
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="event">Event</label>
                        <select name="event" id="event" class="form-control selectjs-basic-single" required>
                            @forelse ($events as $event)
                        <option @if(old('event') == $event->id || app('request')->input('event') == $event->id) selected @endif value="{{$event->id}}">{{$event->name}}</option>
                            @empty
                                <option selected disabled >No Events available.</option>
                            @endforelse
                        </select>
                    </div>
                    <div class="form-group col-sm-12 input-group input-daterange">
                        <input type="text" name="date_from" aria-label="Start Date" placeholder="Start Date" class="form-control" autocomplete="off" required>
                        <div class="input-group-prepend">
                            <span class="input-group-text">To</span>
                        </div>
                        <input type="text" name="date_to" aria-label="End Date" placeholder="End Date" class="form-control" autocomplete="off" required>
                    </div>
                    <div class="form-group col-sm-12 col-md-6">
                        <label for="venue">Venue</label>
                        <input type="text" name="venue" id="venue" class="form-control" value="@if(old('venue')) {{old('venue')}} @endif">
                    </div>
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $('.input-daterange').datepicker({
            format: 'mm/dd/yyyy',
        });
    })

</script>
@endsection
