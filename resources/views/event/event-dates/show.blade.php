@extends('layouts.internal')
@section('title',$event->Event->name)
@section('content')
<div class="page-header">
   <h3 class="mb-2"><a href="{{route('events.show',$event->Event->id)}}">{{$event->Event->name}}</a> <small class="text-black-50 float-right">{{Carbon::parse($event->date_from)->diffForHumans()}}</small></h3>
   <div class="page-breadcrumb">
       <nav aria-label="breadcrumb">
           <ol class="breadcrumb">
               <li class="breadcrumb-item"><a href="{{route('home')}}" class="breadcrumb-link">Dashboard</a></li>
               <li class="breadcrumb-item"><a href="{{route('events.show',$event->Event->id)}}" class="breadcrumb-link">{{$event->Event->name}}</a></li>
               <li class="breadcrumb-item"><a href="#" class="breadcrumb-link">{{$event->isDate()}}</a></li>
           </ol>
       </nav>
   </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5 class="text-muted">Registrations</h5>
                        <h2 class="mb-0"> {{$event->Attendes->count()}}</h2>
                    </div>
                    <div class="float-right icon-circle-medium  icon-box-lg  bg-primary-light mt-1">
                        <i class="fa fa-user fa-fw fa-sm text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5 class="text-muted">Speakers</h5>
                        <h2 class="mb-0"> {{$event->Speakers->count()}}</h2>
                    </div>
                    <div class="float-right icon-circle-medium  icon-box-lg  bg-primary-light mt-1">
                        <i class="fa fa-user fa-fw fa-sm text-primary"></i>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-inline-block">
                        <h5 class="text-muted">Attendance</h5>
                        <h2 class="mb-0"> {{$event->Attendes->where('status',2)->count()}} / {{$event->Attendes->count()}}</h2>
                    </div>
                    <div class="float-right icon-circle-medium icon-box-lg  bg-success-light mt-1">
                       <h5 class="text-success">{{round($event->Attendes->where('status',2)->count() * 100 / max($event->Attendes->count(), 1),2)}} %</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="card card-body">
                <button type="button" data-toggle="modal" data-target="#event-attended-upload-modal" class="btn btn-outline-danger btn-xs">Upload Attendees</button>
                <br>
                <button type="button" data-toggle="modal" data-target="#event-edit-date" class="btn btn-outline-danger btn-xs">Edit this Date</button>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    <div class="float-right btn-group">
                        <a href="{{route('export-event-attendees',[$event->id])}}" class="btn btn-xs btn-outline-info">Export All</a>
                        <a href="#attendies-modal" data-toggle="modal" data-target="#attendies-modal" class="btn btn-xs btn-outline-primary">Add New Attendee</a>
                        <a href="#spot-registration-modal" data-toggle="modal" data-target="#spot-registration-modal" class="btn btn-xs btn-outline-secondary">Spot Registration</a>
                    </div>
                    Attendees
                </div>
                <table class="table table-sm">
                    @forelse ($event->Attendes as $person)
                    <tr>
                        <th><a href="{{route('people.show',$person->Person->id)}}">{{$person->Person->name}}</a></th>
                        <td>
                            {!!$person->Status()!!}
                        </td>
                        <td>
                            <div class="dropdown float-right">
                            <button type="button" class="btn btn-primary btn-xs dropdown-toggle" data-toggle="dropdown">
                                Actions
                            </button>
                            <div class="dropdown-menu">
                                @switch($person->status)
                                    @case(1)
                                    <form action="{{route('attendance.update',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        <input type="hidden" name="status" value="2">
                                        <button type="submit" class="dropdown-item">Mark Attended</button>
                                    </form>
                                    <form action="{{route('attendance.update',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        <input type="hidden" name="status" value="3">
                                        <button type="submit" class="dropdown-item">Cancel</button>
                                    </form>
                                    <form action="{{route('attendance.destroy',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        @csrf
                                        <button type="submit" class="dropdown-item">Delete</button>
                                    </form>
                                    @break
                                    @case(2)
                                    <form action="{{route('attendance.update',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        <input type="hidden" name="status" value="1">
                                        <button type="submit" class="dropdown-item">Mark Not-Attended</button>
                                    </form>
                                    @break
                                    @case(3)
                                    <form action="{{route('attendance.update',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="PUT">
                                        @csrf
                                        <input type="hidden" name="status" value="1">
                                        <button type="submit" class="dropdown-item">Re-Activate</button>
                                    </form>
                                    <form action="{{route('attendance.destroy',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        @csrf
                                        <button type="submit" class="dropdown-item">Delete</button>
                                    </form>
                                    @break
                                    @default
                                    <form action="{{route('attendance.destroy',$person->id)}}" method="post">
                                        <input type="hidden" name="_method" value="DELETE">
                                        @csrf
                                        <button type="submit" class="dropdown-item">Delete</button>
                                    </form>
                                    @endswitch
                            </div>
                            </div>
                            <div class="btn-group btn-group-sm float-right">
                            </div>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <th class="text-center" colspan="4">No Attendees</th>
                    </tr>
                    @endforelse
                </table>
            </div>
        </div>
        <div class="col-sm-12 col-md-6">
            <div class="card">
                <div class="card-header">
                    <a href="#speakers-modal" data-toggle="modal" data-target="#speakers-modal" class="float-right btn btn-xs btn-outline-primary">Add Speakers</a>
                    Speakers
                </div>
                <table class="table table-sm">
                    <tbody>
                        @forelse ($event->Speakers as $speaker)
                            <tr>
                                <th><a href="{{route('people.show',$speaker->Person->id)}}">{{$speaker->Person->name}}</a></th>
                                <th>
                                <form action="{{route('speakers.destroy',$speaker->id)}}" method="post">
                                    <input type="hidden" name="_method" value="DELETE">
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-xs float-right">Delete</button>
                                </form>
                            </tr>
                        @empty
                            <tr>
                                <th colspan="3" class="text-center">No Speakers Attached</th>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="speakers-modal" tabindex="-1" role="dialog" aria-labelledby="speakers-modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
        <form class="modal-body" action="{{route('speakers.store')}}" method="POST">
            <h5 class="modal-title" id="speakers-modalLabel">Add Speakers</h5>
            @csrf
            <div class="row">
               <div class="col-sm-12">
                    <input type="hidden" name="event" value="{{$event->id}}">
                    <div class="form-group">
                        <select name="speaker[]" id="speaker" multiple class="form-control">
                            @forelse ($speakers as $speaker)
                                <option @if ($event->Speakers->contains('id', $speaker->id))) selected @endif value="{{$speaker->id}}">{{$speaker->name}} {{$speaker->email}}</option>
                            @empty
                                
                            @endforelse
                        </select>
                    </div>
                    <div class="float-right">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </div>
            </div>
        </form>
       </div>
    </div>
</div>
<div class="modal" id="attendies-modal" tabindex="-1" role="dialog" aria-labelledby="attendies-modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="modal-body" action="{{route('attendance.store')}}" method="POST">
        <h5 class="modal-title" id="attendies-modalLabel">Add Attendee</h5>
        @csrf
          <div class="row">
            <div class="col-sm-12">
                <input type="hidden" name="event" value="{{$event->id}}">
                <div class="form-group">
                      <select name="person[]" id="person" multiple class="form-control">
                          @forelse ($people as $person)
                              <option @if ($event->Attendes->contains('id', $person->id))) selected @endif value="{{$person->id}}">{{$person->name}} {{$person->email}}</option>
                          @empty
                              
                          @endforelse
                      </select>
                </div>
                <div class="float-right">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
      </form>
    </div>
  </div>
</div>
<div class="modal" id="spot-registration-modal" tabindex="-1" role="dialog" aria-labelledby="spot-registration-modal-modalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form class="modal-body" action="{{route('attendance.spot',[$event->id])}}" method="POST">
        <h5 class="modal-title" id="spot-registration-modal-modalLabel">Add Attendee</h5>
        @csrf
        @include('people.inc.create')
        <div class="col-sm-12">
            <button type="submit" class="btn btn-info btn-sm">Save and Add</button>
        </div>
      </form>
    </div>
  </div>
</div>
    <form action="{{route('event-dates.update',[$event->id])}}" method="POST" class="modal" id="event-edit-date" enctype="multipart/form-data">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-body">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group col-sm-12 input-group input-daterange">
                        <input type="text" name="date_from" aria-label="Start Date" placeholder="Start Date" class="form-control" autocomplete="off" required value="{{$event->FromData()}}">
                        <div class="input-group-prepend">
                            <span class="input-group-text">To</span>
                        </div>
                        <input type="text" name="date_to" aria-label="End Date" placeholder="End Date" class="form-control" autocomplete="off" required value="{{$event->ToData()}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <label for="venue">Venue</label>
                        <input type="text" name="venue" id="venue" class="form-control" value="{{$event->venue}}">
                    </div>
                    <div class="form-group col-sm-12">
                        <button type="submit" class="btn btn-outline-success btn-sm float-right">Save</button>
                    </div>
                </div>
            </div>
        </div>
    </form>

    <form action="{{route('event-dates.attandance.import')}}" method="POST" class="modal" id="event-attended-upload-modal" enctype="multipart/form-data">
        <div class="modal-dialog modal-dialog-centered modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    @csrf
                <input type="hidden" name="event" value="{{$event->id}}">
                <div class="form-group">
                    <input type="file" accept=".csv" required  name="upload" class="form-control">
                </div>
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Cancel</button>
                    <button class="btn btn-info btn-sm" type="submit">Upload</button>
                </div>
            </div>
        </div>
    </form>

<script>
    $(function(){
        $('select').each(function(){
            $(this).select2({width: 'resolve',});
        });
        $('select').css('width', '100%'); 
    })
    $(function(){
        $('.input-daterange').datepicker({
            format: 'dd-mm-yyyy',
        });
    })

</script>
@endsection
